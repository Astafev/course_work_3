package com.coursework.first_version.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column(name = "name")
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    @Column(name = "lastname")
    private String lastname;

    @ManyToMany(mappedBy = "marks", fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private Mark mark;

    public Student(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    public Student() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id &&
                Objects.equals(name, student.name) &&
                Objects.equals(lastname, student.lastname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastname);
    }

    void setMark(Mark mark) {
        this.mark = mark;
    }
    @Override
    public String toString() {
        return "Student: \n name: "
                + name +
                ", lastname: "
                + lastname + ".";
    }
}
