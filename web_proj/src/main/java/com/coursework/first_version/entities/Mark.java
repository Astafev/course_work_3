package com.coursework.first_version.entities;

import javax.persistence.*;

@Entity
@Table(name = "marks")
public class Mark {
    //todo: Mark

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column(name = "mark")
    int mark;

    @ManyToMany(mappedBy = "student", fetch = FetchType.LAZY)
    @JoinColumn(name = "stud_id")
    private Student student;

    @ManyToMany(mappedBy = "course", fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    private Course course;

    public Mark(int mark) {
        this.mark = mark;
    }

    public Mark(){
    }

    @Override
    public String toString() {
        return "Student (name = " + student.getName() +
                ", lastname = "+ student.getLastname() +
                ") has mark " + mark +
                " for course named" + course.getName() + ".";
    }
}
